Title: 9. Procedural Logics
page_order: 9

## Other projects
- The  Project  Formerly  Known  as  Kindle  Forkbomb Printing Press: http://www.arnolfini.org.uk:7081/project.arnolfini/the-project-formerly-known-as-kindle-forkbomb

## References for thinking
- Ensmenger, Nathan. “The Multiple Meanings of a Flowchart.” Information & Culture: A Journal of History 51, no. 3 (2016): 321–51. https://doi.org/10.1353/lac.2016.0013.
- Morris, Stephen, and Orlena Gotel. “The Role of Flow Charts in the Early Automation of Applied Mathematics.” BSHM Bulletin: Journal of the British Society for the History of Mathematics 26, no. 1 (March 2011): 44–52. https://doi.org/10.1080/17498430903449207.
