Title: TOC
page_order: -2

**Title:** Aesthetic Programming: A Handbook of Software Studies, or Software Studies for Dummies

**By**: Winnie Soon and Geoff Cox

## 0. Preface

## Chapter 1: Getting started
* What is Javascript and why p5.js?
* Setting up and Reading code and syntax
* Hello world syntax
* Free and Open-source software and culture
* Mini-ex 1: Think About My First Program

## Chapter 2: Variable Geometry
* Multi by David Reinfurt (cf. sundial project for next chapter)
* Basics: coordination, variables, geometry, shapes/patterns, faces (with p5.playground)
* Having fun, and not just for fun, with software, free speech and injurious words, bad jokes
* Politics of emoticons, politics of representation, background of unicode and universal languages
* Mini-ex 2: A fun face

## Chapter 3: Infinite loops
* Artwork: Asterisk Painting (2013) by Bell John
* Iterations: For/ While Loops
* Conditional statements and Functions
* Time related syntax, machine and human time (unix-epoch), clocks
* Transform: translate(), rotate(), push/pop()
* Temporalities: real-time, machine time, micro-time and just-in-time (live) coding (Ernst's essay - if... then - loop forever)
* Mini-ex 3: Design a Throbber differently

## Chapter 4: Data Capture
* Artwork: How We Act Together (2016) by Lauren McCarthy and Kyle McDonald (mention more in passing, not too much emphasis)
* Mouse and Keyboard events
* Web camera inputs: with clmtrackr.js library
* Audio inputs: with p5.sound library
* Tracking: The datafication of everything, including your habits (datafied research).
* Mini-ex 4: CAPTURE ALL: Feedback machines

## Chapter 5: Object Orientation
* Design work: TOFU GO by Francis Lam that inspires Winnie's simple version 
* Objects, Class, Behaviors and Arguments
* Play objects: with p5.play library
* Object-oriented thinking: cooking and coding with class struggle (food and recipes, cultural difference)
* Mini-ex 5: Games with objects

## Chapter 6: Automatisms
* The Game of Life (1970) by John Horton Conway / Langton's ant (1986) by Chris Langton + [automata I](https://isohale.com/Development-1) by Catherine Griffiths  
* (Commodore 64 Basic program: 10 PRINT)
* Random(), Noise()
* Rule-based and Emergent systems
* Generators and other automatisms (genotypes), love letter generators
* Mini-ex 6: A generative program

## Chapter 7: Vocable Code
* Artwork: Vocable Code by Winnie Soon
* Textuality: typography, text size, alignment and style
* Parsing JSON text files
* Codework: attention to names and poetics
* Speaking and Queer Code
* Mini-ex 7: Write and Generate an electronic literature

## Chapter 8: Que(e)ries
* Artwork: Net Art Generator by Cornelia Sollfrank (authorship and copyright, image encryption)
* Image processing: fetching, loading and display
* Application Programming Interfaces
* Data, Archives, Repositories, Libraries, Databases / Critical APIs across time (googlification of everything, literacy)
* Mini-ex 8: Working with APIs

## Chapter 9: Flow Chart and Diagramming
* Projects: The Status Project by Heath Bunting (in passing) + a series of flow charts (inc. vocable code)
* cf. table of contents and other drawn diagrams (dean kenning's jackson 5 flowchart)
* Receipts,  Procedures, Algorithms
* Mini-ex 9: Representing a program with a flowchart (check algorithm by Goffey is in refs)

## Chapter 10: Machine Learning
* Personal assistants: Eliza (1964-1966) + elizabot.js version (2005) by Norbert Landsteiner  
* What is Machine Learning?
* Machine prediction: Classification and Text generation
* Experimenting ml5.js library
* Ways of Machine Seeing, Reading and Processing

## Chapter 11: Programming Aesthetics/Aesthetic Programming
* Machine learning generated text – based on the text in this book

## Indexing
reference to: https://monoskop.org/media/text/? and Linda's work in Exe practice
