Title: 8. Que(e)ries
page_order: 8

- politics of API
- appropriation and copyright
- queer queries (the code and image keyword could be 'queer')
- terms and conditions -> regulation
- operators -> selection, ranking, relevance, curation

- reference:
Sollfrank PhD thesis 